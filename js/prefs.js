TDN.prefs = (function () {

    // THEME

    var lightTheme = 'css/todonna-bulma-light.css';
    var darkTheme = 'css/todonna-bulma-dark.css';

    function switchTheme(theme) {
        var themeLink = document.getElementById('theme-link');
        var newThemeLink = document.createElement('link');
        newThemeLink.id = 'theme-link';
        newThemeLink.rel = 'stylesheet';
        newThemeLink.href = theme;

        themeLink.parentNode.replaceChild(newThemeLink, themeLink);
    }

    function saveTheme(event) {
        let themeRadioButtons = document.getElementsByName('theme');
        let selectedTheme = '';

        for (let i = 0; i < themeRadioButtons.length; i++) {
            if (themeRadioButtons[i].checked) {
                selectedTheme = themeRadioButtons[i].value;
                break;
            }
        }

        switchTheme(selectedTheme);

        localStorage.setItem('todonna-theme', selectedTheme);
    }

    function loadTheme() {

        let selectedTheme = localStorage.getItem('todonna-theme') || 'css/todonna-bulma-light.css';

        document.querySelector('input[name="theme"][value="' + selectedTheme + '"]').checked = true;

        switchTheme(selectedTheme);

    }

    return {
        loadTheme,
        saveTheme
    };
})();
