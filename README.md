# todonna.gitlab.io

A simple TODO / Shopping list app without bloat or ads.

![Todonna](https://mobilohm.gitlab.io/img/shots/todonna.png)

This is a [PWA](https://en.wikipedia.org/wiki/Progressive_web_app), simply visit [Todonna](https://todonna.gitlab.io/) with your - both desktop or mobile -  browser and use "Install Todonna" to have it available even when offline.

If you find one of the [MobilOhm](https://mobilohm.gitlab.io/) apps helpful and would like to support its development, consider making a contribution through [Ko-fi](https://ko-fi.com/yphil) or [Liberapay](https://liberapay.com/yPhil/).

Your support helps keep this app free, open source, and ad-free.
